<?php

use App\Channel;
use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Channel::create([
            'name'  => 'Laravel 7',
            'slug'  =>  Str::slug('Laravel 7')
        ]);

        Channel::create([
            'name'  => 'Vue Js 3',
            'slug'  => Str::slug('Vue Js 3')
        ]);

        Channel::create([
            'name'  => 'Angular 7',
            'slug'  => Str::slug('Angular 7')
        ]);

        Channel::create([
            'name'  => 'Nodejs',
            'slug'  => Str::slug('Nodejs')
        ]);
    }
}
