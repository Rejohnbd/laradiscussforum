@extends('layouts.app')

@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">Add Discussion</div>

        <div class="card-body">
        <form action="{{ route('discussions.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="">
            </div>
            
            <div class="form-group">
              <label for="content">Content</label>
              <input id="content" type="hidden" name="content">
              <trix-editor input="content"></trix-editor>
            </div>

            <div class="form-group">
              <label for="channel">Channel</label>
              <select class="form-control" name="channel" id="channel">
                @foreach ($channels as $channel)
                    <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" class="btn btn-success">Create Discussion</button>
        </form>
        </div>
    </div>
</div>
@endsection
