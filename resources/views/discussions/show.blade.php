@extends('layouts.app')

@section('content')
<div class="col-md-12">
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
</div>
<div class="col-md-12">
    <div class="card mb-2">
        @include('partials.discussion-header')
        <div class="card-body">
            <div class="text-center">
                <strong> {{ $discussion->title }}</strong>
            </div>
            <hr/>
            {!! $discussion->content !!}
            @if($discussion->bestReply)
                <div class="card bg-success mt-2">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img width="30px" height="30px" style="border-radius: 50%" src="{{ Gravatar::src($discussion->bestReply->owner->email) }}" alt="">
                                <strong class="ml-2">{{ $discussion->bestReply->owner->name }}</strong>
                            </div>
                            <div>BEST REPLY</div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! $discussion->bestReply->content !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
    
    <p class="text-info">Replies</p>
    <hr/>
    @foreach($discussion->replies()->paginate(3) as $reply)
        <div class="card mb-1">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <div>
                        <img width="30px" height="30px" style="border-radius: 50%" src="{{ Gravatar::src($reply->owner->email) }}" alt=""/>
                        <strong class="ml-2">{{ $reply->owner->name }}</strong>
                    </div>
                    <div>
                        @auth
                            @if(auth()->user()->id === $discussion->user_id)
                            <form action="{{ route('discussions.best-reply', ['discussion' => $discussion->slug, 'reply' => $reply->id ]) }}" method="POST">
                                @csrf 
                                <button type="submit" class="btn btn-sm btn-primary">Mark as best reply</button>
                            </form>
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
            <div class="card-body">
                {!! $reply->content !!}
            </div>
        </div>
    @endforeach 
        {{ $discussion->replies()->paginate(3)->links() }}

    <div class="card mt-2">
        <div class="card-header">Add a reply</div>
        <div class="card-body">
            @auth
                <form action="{{ route('replies.store', $discussion->slug) }}" method="POST">
                    @csrf
                    <input type="hidden" name="content" id="content">
                    <trix-editor input="content"></trix-editor>
                    <button type="submit" class="btn btn-success mt-2">Add Reply</button>
                </form>
            @else
                <a href="{{ route('login') }}" class="btn btn-info text-center">Sign In to Add Reply</a>    
            @endauth
        </div>
    </div>    
</div>
@endsection
