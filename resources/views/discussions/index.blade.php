@extends('layouts.app')

@section('content')
<div class="col-md-12">
    @if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
    @endif
</div>

<div class="col-md-12">
    @foreach ($discussions as $discussion)
    <div class="card mb-2">
        @include('partials.discussion-header')
        <div class="card-body">
            <div class="text-center">
                <strong> {{ $discussion->title }}</strong>
            </div>
        </div>
    </div>
    @endforeach
    {{ $discussions->appends(['channel' => request()->query('channel')])->links() }}
</div>
@endsection